# Quick Prototypize containers
The idea is to create a docker compose to quick prototype software with:

* HaProxy (with roxy wi interface): as load balancer to manage service to have one entry point
* Keycloack: for login
* Hasura: for graphql API and DB management
* Wordpress + Figma: to create mockup and quick static webpage
* [Optional] Metabase: for chart dashboarding
* [Optional] Streamlit: for custo coded UI
* [Optional] Frontend: something to create web component and porting the static website to working one
* [Optional] Micro frontend: some micro frontend framework that can remove wordpress use to merge all component
* [Optional] Backend:something to create customized API with logic

![](imgs/quick_rpototype.drawio.png)

## References
* [Figma + Wordpress](https://wordpress.org/news/2021/04/getting-started-with-the-figma-wordpress-design-library/)
* [Keycloak + Wordpress](https://medium.com/webeetle/keycloak-e-wordpress-555d0c2cfec7)