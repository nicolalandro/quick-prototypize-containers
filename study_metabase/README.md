# Metabase + Postgres + PGAdmin
This project whant to made a docker compose to create quickly dash application using Metabase.
Contains:
* [x] Postgres
* [x] PgAdmin
* [x] Metabase
* [x] Hasura
* [ ] Keycloack
* [ ] CMS/Microfrontend mciro-lc (something to create page)
* [ ] Streamlit
* [ ] Appwrite/Supabase? 

## How to run

* Run all
```
docker-compose up
```
* pg admin at loacalhost:5050
* metabase at localhost:3000

## References
* [Postgres + PGAdmin docker](https://towardsdatascience.com/how-to-run-postgresql-and-pgadmin-using-docker-3a6a8ae918b5)
* [Metabase docker](https://www.metabase.com/docs/latest/installation-and-operation/running-metabase-on-docker)
* [Keycloack + Metabase](https://www.metabase.com/docs/latest/people-and-groups/saml-keycloak)